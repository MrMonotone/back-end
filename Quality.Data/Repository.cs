﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quality.Core.Entities;
using Quality.Core.Interfaces;

namespace Quality.Data
{
    public class Repository : IServerRepository
    {
        private string _connectionString;
        public Repository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException("connectionstring");
            }
            _connectionString = connectionString;
        }

        /// <summary>
        /// Gets the Qualified Parts in a paged format
        /// </summary>
        /// <param name="pageNumber">Page number to retrieve</param>
        /// <param name="pageSize">How many records to retrieve per page</param>
        /// <returns>List of Qualified Parts</returns>
        public async Task<List<QualifiedPart>> GetQualifiedPartsPagedAsync(int pageNumber, int pageSize)
        {
            // These methods should be split into partial classes as more information is added
            List<QualifiedPart> retValue = new List<QualifiedPart>();

            using (SqlConnection dc = new SqlConnection(_connectionString))
            {
                DataSet ds = null;

                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(new SqlParameter("PageNumber", pageNumber));
                parms.Add(new SqlParameter("PageSize", pageSize));

                ds = await SQLHelper.ExecuteQueryAsync(_connectionString, "GetQualityPartsListPaged", parms);

                retValue = CreateQualifiedPartsFromDataTable(ds.Tables[0]);
            }

            return retValue;
        }

        private List<QualifiedPart> CreateQualifiedPartsFromDataTable(DataTable dataTable)
        {
            List<QualifiedPart> retValue = new List<QualifiedPart>();
            QualifiedPart part;
            foreach (DataRow dr in dataTable.Rows)
            {
                part = CreateQualifiedPartFromDataRow(dr);
                retValue.Add(part);
            }
            return retValue;
        }

        private QualifiedPart CreateQualifiedPartFromDataRow(DataRow dr)
        {
            return new QualifiedPart()
            {
                IsQualified = dr.GetValueOrDefault<bool>("IsQualified"),
                PartNumber = dr.GetValueOrDefault<string>("PartNumber"),
                Revision = dr.GetValueOrDefault<string>("Revision"),
                PartName = dr.GetValueOrDefault<string>("ToolDieSetNumber"),
                OpenPo = dr.GetValueOrDefault<bool>("OpenPo"),
                PartJurisdiction = dr.GetValueOrDefault<string>("PartJurisdiction"),
                PartClassification = dr.GetValueOrDefault<string>("PartClassification"),
                SupplierCompanyName = dr.GetValueOrDefault<string>("SupplierName"),
                SupplierCompanyCode = dr.GetValueOrDefault<string>("SupplierCode"),
                Ctq = dr.GetValueOrDefault<bool>("Ctq"),
                CreatedBy = dr.GetValueOrDefault<int>("CreatedById"),
                CreatedDate = dr.GetValueOrDefault<DateTime>("CreatedDateUtc"),
                LastUpdatedBy = dr.GetValueOrDefault<int>("LastUpdatedById"),
                LastUpdatedDate = dr.GetValueOrDefault<DateTime>("LastUpdatedDateUtc"),
                ExpiresDate = dr.GetValueOrNullable<DateTime>("ExpirationDateUtc"),
            };
        }
    }

}
