﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Quality.Data
{
    public static class SQLHelper
    {
        #region methods

        #region public      

        public static DataSet ExecuteQuery(string connectionString, string query)
        {
            return ExecuteQuery(connectionString, query, null);
        }

        public static DataSet ExecuteQuery(string connectionString, string query, List<SqlParameter> parameters)
        {
            Task<DataSet> retValue = Task.Run(async () => { DataSet ds = await ExecuteQueryAsync(connectionString, query, parameters); return ds; });
            return retValue.Result;
        }

        public static void ExecuteNonQuery(string connectionString, string query)
        {
            Task.Run(async () => await ExecuteNonQueryAsync(connectionString, query));
        }

        public async static Task ExecuteNonQueryAsync(string connectionString, string query)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);

                try
                {
                    command.Connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
                catch (SqlException ex)
                {
                    // This is where we would log stuff if we had logging
                    Debug.WriteLine(string.Format("ExecuteNonQuery on ***{0}*** - {1}", query, ex.Message));
                }
                catch (Exception ex)
                {
                    // This is where we would log stuff if we had logging
                    Debug.WriteLine(string.Format("ExecuteNonQuery on ***{0}*** - {1}", query, ex.Message));
                }
            }
        }

        public async static Task ExecuteNonQueryStoredProcedureAsync(string connectionString, string procName, List<SqlParameter> parms)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var p in parms)
                    {
                        command.Parameters.Add(p);
                    }
                    command.Connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
                catch (SqlException ex)
                {
                    // This is where we would log stuff if we had logging
                    Debug.WriteLine(string.Format("ExecuteNonQuery on ***{0}*** - {1}", procName, ex.Message));
                }
                catch (Exception ex)
                {
                    // This is where we would log stuff if we had logging
                    Debug.WriteLine(string.Format("ExecuteNonQuery on ***{0}*** - {1}", procName, ex.Message));
                }
            }
        }

        public async static Task ExecuteTransactionNonQueryStoredProcedureAsync(SqlTransaction transaction, string procName, List<SqlParameter> parms)
        {
            try
            {
                SqlCommand command = new SqlCommand(procName, transaction.Connection, transaction);
                command.CommandType = CommandType.StoredProcedure;
                foreach (var p in parms)
                {
                    command.Parameters.Add(p);
                }

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                await command.ExecuteNonQueryAsync();
            }
            catch (SqlException ex)
            {
                // This is where we would log stuff if we had logging
                Debug.WriteLine(string.Format("ExecuteNonQuery on ***{0}*** - {1}", procName, ex.Message));
            }
            catch (Exception ex)
            {
                // This is where we would log stuff if we had logging
                Debug.WriteLine(string.Format("ExecuteNonQuery on ***{0}*** - {1}", procName, ex.Message));
            }
        }

        public async static Task<DataSet> ExecuteQueryAsync(string connectionString, string query)
        {
            return await ExecuteQueryAsync(connectionString, query, null);
        }

        public async static Task<DataSet> ExecuteQueryAsync(string connectionString, string query, List<SqlParameter> parameters)
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);

                if (parameters != null && parameters.Count > 0)
                {
                    command.Parameters.AddRange(parameters.ToArray());
                }

                command.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                {
                    Task t = Task.Run(() =>
                    {
                        try
                        {
                            dataAdapter.Fill(dataSet);
                        }
                        catch (SqlException ex)
                        {
                            // This is where we would log stuff if we had logging
                            Debug.WriteLine(string.Format("ExecuteQueryAsync on ***{0}*** - {1}", query, ex.Message));
                        }
                    });

                    try
                    {
                        await t;
                    }
                    catch (SqlException ex)
                    {
                        // This is where we would log stuff if we had logging
                        Debug.WriteLine(string.Format("ExecuteQueryAsync on ***{0}*** - {1}", query, ex.Message));
                    }
                    catch (Exception ex)
                    {
                        // This is where we would log stuff if we had logging
                        Debug.WriteLine(string.Format("ExecuteQueryAsync on ***{0}*** - {1}", query, ex.Message));
                    }
                }
            }
            return dataSet;
        }
    }
    #endregion public
    #endregion methods
}