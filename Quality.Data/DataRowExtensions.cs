﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quality.Data
{
    // DataRow extension methods
    // 
    //     GetNotNullValue      - Throws exception if column is nullable as opposed to the value being NULL
    //     GetValueOrDefault    - Returns value or a default value if NULL
    //     GetValueOrNull       - Returns null rather than DbNull
    //     GetValueOrNullable   - Returns Nullable<T> rathern than DbNull
    public static class DataRowExtensions
    {
        #region Column Index Extensions
        public static T GetValueOrDefault<T>(this DataRow row, int columnIndex)
        {
            T value = default(T);

            if (value is string)
                value = (T)(object)row.GetValueOrDefault(columnIndex, string.Empty);
            else
                value = row.GetValueOrDefault(columnIndex, default(T));

            return value;
        }

        public static T GetValueOrDefault<T>(this DataRow row, int columnIndex, T defaultValue)
        {
            T value = defaultValue;

            if (row[columnIndex] != DBNull.Value)
            {
                value = (T)row[columnIndex];
            }
            return value;
        }

        public static T GetValueOrNull<T>(this DataRow row, int columnIndex) where T : class
        {
            T value = null;

            if (row[columnIndex] != DBNull.Value)
            {
                value = (T)row[columnIndex];
            }
            return value;
        }

        public static Nullable<T> GetValueOrNullable<T>(this DataRow row, int columnIndex) where T : struct
        {
            if (row[columnIndex] == DBNull.Value)
                return null;
            else
                return (T)row[columnIndex];
        }
        #endregion Column Index Extensions

        #region Column Name Extensions
        public static T GetValueOrDefault<T>(this DataRow row, string columnName)
        {
            T value = default(T);

            if (value is string)
                value = (T)(object)row.GetValueOrDefault(columnName, string.Empty);
            else
                value = row.GetValueOrDefault(columnName, default(T));

            return value;
        }

        public static T GetValueOrDefault<T>(this DataRow row, string columnName, T defaultValue)
        {
            T value = defaultValue;

            if (row[columnName] != DBNull.Value)
            {
                value = (T)row[columnName];
            }
            return value;
        }

        public static T GetValueOrNull<T>(this DataRow row, string columnName) where T : class
        {
            T value = null;

            if (row[columnName] != DBNull.Value)
            {
                value = (T)row[columnName];
            }
            return value;
        }

        public static Nullable<T> GetValueOrNullable<T>(this DataRow row, string columnName) where T : struct
        {
            Nullable<T> retValue = null;

            if (row[columnName] != DBNull.Value)
            {
                return (T)row[columnName];
            }

            return retValue;
        }
        #endregion Column Name Extensions

    }
}
