﻿CREATE TABLE [dbo].[Parts] (
    [PartId]         INT            IDENTITY (1, 1) NOT NULL,
    [CompanyId]      INT            NOT NULL,
    [PartNumber]     NVARCHAR (100) NOT NULL,
    [PartName]       NVARCHAR (200) NULL,
    [Jurisdiction]  NVARCHAR (100) NULL,
    [Classification] NVARCHAR (100) NULL,
    CONSTRAINT [PK_Parts] PRIMARY KEY CLUSTERED ([PartId] ASC),
    CONSTRAINT [FK_Parts_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Companies] ([Id])
);