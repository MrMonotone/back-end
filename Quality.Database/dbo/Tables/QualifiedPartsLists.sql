﻿CREATE TABLE [dbo].[QualifiedPartsLists] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [IsQualified]        BIT            NOT NULL,
    [PartId]             INT            NOT NULL,
    [Revision]           NVARCHAR (100) NULL,
    [ToolDieSetNumber]   VARCHAR (40)   NOT NULL,
    [OpenPo]             BIT            CONSTRAINT [DF_QualifiedPartsList_OpenPo] DEFAULT ((1)) NOT NULL,
    [CustomerCompanyId]  INT            NOT NULL,
    [SupplierCompanyId]  INT            NOT NULL,
    [CreatedById]        INT            NOT NULL,
    [CreatedDateUtc]     DATETIME       NOT NULL,
    [LastUpdatedById]    INT            NOT NULL,
    [LastUpdatedDateUtc] DATETIME       NOT NULL,
    [ExpirationDateUtc]  DATETIME       NULL,
    [Ctq]                BIT            NOT NULL,
    CONSTRAINT [PK_QualifiedPartsList_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualifiedPartsList_CustomerCompanyId] FOREIGN KEY ([CustomerCompanyId]) REFERENCES [dbo].[Companies] ([Id]),
    CONSTRAINT [FK_QualifiedPartsList_SupplierCompanyId] FOREIGN KEY ([SupplierCompanyId]) REFERENCES [dbo].[Companies] ([Id]),
    CONSTRAINT [FK_QualifiedPartsList_PartId] FOREIGN KEY ([PartId]) REFERENCES [dbo].[Parts] ([PartId]),
    CONSTRAINT [FK_QualifiedPartsList_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[Users] ([Id]),
    CONSTRAINT [FK_QualifiedPartsList_LastUpdatedById] FOREIGN KEY ([LastUpdatedById]) REFERENCES [dbo].[Users] ([Id]),
	CONSTRAINT [UK_QualifiedPartsList_TDS] UNIQUE(PartId, Revision, CustomerCompanyId, SupplierCompanyId, ToolDieSetNumber)
);