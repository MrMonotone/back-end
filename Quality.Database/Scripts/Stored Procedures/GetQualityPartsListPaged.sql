﻿CREATE PROCEDURE [dbo].[GetQualityPartsListPaged]
	@PageNumber int = 1,
	@PageSize int = 20
AS
BEGIN
	SELECT 
		IsQualified, 
		PartNumber,
		PartName,
		Revision,
		ToolDieSetNumber,
		OpenPo,
		Jurisdiction as PartJurisdiction,
		Classification as PartClassification,
		CompanyName as SupplierName,
		SupplierCodes as SupplierCode,
		Ctq,
		CreatedById,
		CreatedDateUtc,
		LastUpdatedById,
		LastUpdatedDateUtc,
		ExpirationDateUtc
	FROM 
		QualifiedPartsLists qpl
	JOIN 
		Parts p
	ON 
		qpl.PartId = p.PartId
	JOIN 
		CustomerSupplierXref x
	ON 
		qpl.SupplierCompanyId = x.SupplierCompanyId
	JOIN 
		Companies c
	ON 
		qpl.SupplierCompanyId = c.Id
	ORDER BY 
		PartNumber ASC
	OFFSET @PageSize * (@PageNumber - 1) ROWS
	FETCH NEXT @PageSize ROWS ONLY
END
