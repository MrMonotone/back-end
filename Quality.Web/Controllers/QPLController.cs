﻿using Quality.Core.Entities;
using Quality.Core.Interfaces;
using Quality.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Quality.Web.Controllers
{
    public class QPLController : Controller
    {
        // We would use processor interfaces if we had more complex data flow, we could also pass a logger here as well
        private IServerRepository Repository
        {
            get
            {
                return new Repository(ConfigurationManager.ConnectionStrings["QualityLocal"].ConnectionString);
            }
        }

        //// Synchronous 
        //public JsonResult GetQualifiedPartsPaged(int pageNumber = 1, int pageSize = 20)
        //{
        //    List<QualifiedPart> parts = new List<QualifiedPart>();

        //    Task t = Task.Run(async () => parts = await Repository.GetQualifiedPartsPagedAsync(pageNumber, pageSize));
        //    t.Wait();
        //    JsonResult json = Json(parts, JsonRequestBehavior.AllowGet);
        //    return json;
        //}

        // Asynchronous
        public async Task<JsonResult> GetQualifiedPartsPaged(int pageNumber = 1, int pageSize = 20)
        {
            List<QualifiedPart> parts = await Repository.GetQualifiedPartsPagedAsync(pageNumber, pageSize);
            JsonResult json = Json(parts, JsonRequestBehavior.AllowGet);
            return json;
        }

    }
}
