# ASP.Net Software Engineer Exam for Net-Inspect #

### What is this repository for? ###

This repository Serves as a starting project for a coding challenge designed for ASP.Net back-end software engineers. 

It is consisted of a SQL Server Database Project and an MVC project. The SQL project contains the initial schema and post deployment scripts to populate the database with some initial data.

### How do I get set up? ###

* Pull a copy of the master branch.
* The solution was created using Visual Studio 2017. If you currently do not have that version or cannot use an earlier version to open the solution, contact your Net-Inspect hiring manager.
* Make sure the project builds correctly.
* If you don't have SQL Server installed on your machine, you can now download the most recent version of SQL Server Developer Edition for free. 
* Deploy the database project to the local instance of SQL server that you will be using and make sure the database is usable and contains preliminary data.
* You are now all set to start your work.

### Contribution guidelines ###

In an initial email, you must have received 2 documents that describe the details of the coding challenge.

1.	statement of work - part 1.pdf
2.	qpl mockups - part 1.pdf

Please read the documents in order and follow instructions described in the statement of work.

### Who do I talk to? ###

* Sasan Boostani: sasan.boostani@net-inspect.com