﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Quality.Core.Entities;

namespace Quality.Core.Interfaces
{
    public interface IServerRepository
    {
        Task<List<QualifiedPart>> GetQualifiedPartsPagedAsync(int pageNumber, int pageSize);
    }
}
