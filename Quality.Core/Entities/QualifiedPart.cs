﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quality.Core.Entities
{
    public class QualifiedPart
    {
        public bool IsQualified { get; set; }
        public string PartNumber { get; set; }
        public string Revision { get; set; }
        public string PartName { get; set; }
        public string ToolDieSetNumber { get; set; }
        public bool OpenPo { get; set; }
        public string PartJurisdiction { get; set; }
        public string PartClassification { get; set; }
        public string SupplierCompanyName { get; set; }
        public string SupplierCompanyCode { get; set; }
        public bool Ctq { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public DateTime? ExpiresDate { get; set; }
    }

}
